from django.urls import path

from sysfacturation_app1.views import Home

app_name = 'sysfacturation_app1'

urlpatterns = [
    path('', Home.as_view(), name="home")
]